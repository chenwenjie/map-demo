import React, { Component } from 'react'
import './App.css'
import { Map } from './Map'
import ConfigDrawer from './ConfigDrawer'
import { MuiThemeProvider } from 'material-ui'

class App extends Component {
  render () {
    return (
      <MuiThemeProvider>
        <div className="container">
          <ConfigDrawer/>
          <Map/>
        </div>
      </MuiThemeProvider>
    )
  }
}

export default App
