import data from './Crunchbase'

let formattedData = data.entities
  .map(t => t.properties)
  .map(t => ({
    cat: t.categories ? t.categories[0].value : 'Unknown',
    name: t.identifier.value.split(' ').join(';'),
    value: 20,
    icon: 'https://crunchbase-production-res.cloudinary.com/image/upload/' + t.identifier.image_id,
  }))

export default formattedData
