import data from './Default'

let formattedData = data
  .map(t => ({
    ...t,
    name: t.name.split(' ').join(';'),
    value: 20,
    icon: ''
  }))

export default formattedData
