import React from 'react'
import Drawer from 'material-ui/Drawer'
import { RaisedButton, Tab, Tabs, TextField } from 'material-ui'
import { renderSvg } from './Map'
import './App.css'
import CranchbaseData from './CrunchbaseData'
import DefaultData from './DefaultData'

export default class ConfigDrawer extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      data: props.data || [],
      name: '',
      cat: '',
      value: 20,
      icon: '',
    }

    this.updateName = this.updateName.bind(this)
    this.updateCat = this.updateCat.bind(this)
    this.updateValue = this.updateValue.bind(this)
    this.updateIcon = this.updateIcon.bind(this)
    this.renderMap = this.renderMap.bind(this)
    this.loadDefault = this.loadDefault.bind(this)
    this.loadCrunchbase = this.loadCrunchbase.bind(this)
    this.reset = this.reset.bind(this)
  }

  componentDidMount () {
    this.loadDefault()
  }

  updateName (event) {
    this.setState({...this.state, name: event.target.value})
  }

  updateCat (event) {
    this.setState({...this.state, cat: event.target.value})
  }

  updateValue (event) {
    this.setState({...this.state, value: event.target.value})
  }

  updateIcon (event) {
    this.setState({...this.state, icon: event.target.value})
  }

  renderMap () {
    let t = {
      name: this.state.name,
      cat: this.state.cat,
      value: this.state.value,
      icon: this.state.icon,
    }
    this.setState({
      ...this.state,
      name: '',
      cat: '',
      value: 20,
      icon: '',
      data: [...this.state.data, t]
    }, () => renderSvg(this.state.data))
  }

  loadDefault () {
    this.setState({
      ...this.state,
      data: DefaultData,
    }, () => renderSvg(this.state.data))
  }

  loadCrunchbase () {
    this.setState({
      ...this.state,
      data: CranchbaseData,
    }, () => renderSvg(CranchbaseData))
  }

  reset () {
    this.setState({
      ...this.state,
      data: [],
    }, () => renderSvg([]))
  }

  render () {
    return (
      <div>
        <Drawer width={300} openSecondary={true} open={true}>
          <Tabs>
            <Tab label="Load from Source">
              <div className="config-container">
                <RaisedButton
                  label="Load Default"
                  fullWidth={true}
                  primary={true}
                  className="load-btn"
                  onClick={this.loadDefault}
                />
                <RaisedButton
                  label="Load Crunchbase"
                  fullWidth={true}
                  secondary={true}
                  className="load-btn"
                  onClick={this.loadCrunchbase}
                />
                <RaisedButton
                  label="Clear"
                  fullWidth={true}
                  className="load-btn"
                  onClick={this.reset}
                />
              </div>
            </Tab>
            <Tab label="Add Individual">
              <div className="config-container">
                <TextField
                  id="name"
                  floatingLabelText="Company Name"
                  value={this.state.name}
                  onChange={this.updateName}
                /><br/>
                <TextField
                  id="cat"
                  floatingLabelText="Category"
                  value={this.state.cat}
                  onChange={this.updateCat}
                /><br/>
                <TextField
                  id="value"
                  floatingLabelText="Display Size"
                  type="number"
                  value={this.state.value}
                  onChange={this.updateValue}
                /><br/>
                <TextField
                  id="icon"
                  floatingLabelText="Icon Path (Optional)"
                  value={this.state.icon}
                  onChange={this.updateIcon}
                /><br/><br/>
                <RaisedButton
                  label="Add Company"
                  fullWidth={true}
                  primary={true}
                  className="load-btn"
                  onClick={this.renderMap}
                />
              </div>
            </Tab>
          </Tabs>
        </Drawer>
      </div>
    )
  }
}