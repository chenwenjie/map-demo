import React from 'react'
import * as d3 from 'd3'
import { legendColor } from 'd3-svg-legend'

const renderSvg = (data) => {
  let svg = d3.select('#svg')
  svg.selectAll('*').remove()
  if (data.length === 0) return
  let width = svg.property('clientWidth') // get width in pixels
  let height = +svg.attr('height')
  let centerX = width * 0.5
  let centerY = height * 0.5
  let strength = 0.05

  let scaleColor = d3.scaleOrdinal(d3.schemeCategory20)

  let pack = d3.pack()
    .size([width, height])
    .padding(1.5)

  let forceCollide = d3.forceCollide(d => d.r + 1)

  let simulation = d3.forceSimulation()
    .force('charge', d3.forceManyBody())
    .force('collide', forceCollide)
    .force('x', d3.forceX(centerX).strength(strength))
    .force('y', d3.forceY(centerY).strength(strength))

  if ('matchMedia' in window && window.matchMedia('(max-device-width: 767px)').matches) {
    data = data.filter(el => {
      return el.value >= 50
    })
  }

  let root = d3.hierarchy({children: data})
    .sum(d => d.value)

  let nodes = pack(root).leaves().map(node => {
    const data = node.data
    return {
      x: centerX + (node.x - centerX) * 3, // magnify start position to have transition to center movement
      y: centerY + (node.y - centerY) * 3,
      r: 0, // for tweening
      radius: node.r, //original radius
      id: data.cat + '.' + (data.name.replace(/\s/g, '-')),
      cat: data.cat,
      name: data.name,
      value: data.value,
      icon: data.icon,
    }
  })
  simulation.nodes(nodes).on('tick', ticked)

  // svg.style('background-color', '#eee')
  let node = svg.selectAll('.node')
    .data(nodes)
    .enter().append('g')
    .attr('class', 'node')
    .call(d3.drag()
      .on('start', (d) => {
        if (!d3.event.active) { simulation.alphaTarget(0.2).restart() }
        d.fx = d.x
        d.fy = d.y
      })
      .on('drag', (d) => {
        d.fx = d3.event.x
        d.fy = d3.event.y
      })
      .on('end', (d) => {
        if (!d3.event.active) { simulation.alphaTarget(0) }
        d.fx = null
        d.fy = null
      }))

  node.append('circle')
    .attr('id', d => d.id)
    .attr('r', 0)
    .style('fill', d => scaleColor(d.cat))
    .transition().duration(2000).ease(d3.easeElasticOut)
    .tween('circleIn', (d) => {
      let i = d3.interpolateNumber(0, d.radius)
      return (t) => {
        d.r = i(t)
        simulation.force('collide', forceCollide)
      }
    })

  node.append('clipPath')
    .attr('id', d => `clip-${d.id}`)
    .append('use')
    .attr('xlink:href', d => `#${d.id}`)

  node.filter(d => !String(d.icon).includes('img/') && !String(d.icon).includes('http://') && !String(d.icon).includes('https://'))
    .append('text')
    .classed('node-icon', true)
    .attr('clip-path', d => `url(#clip-${d.id})`)
    .selectAll('tspan')
    .data(d => d.name.split(';'))
    .enter()
    .append('tspan')
    .attr('x', 0)
    .attr('y', (d, i, nodes) => (20 + (i - nodes.length / 2 - 0.5) * 16))
    .text(name => name)

  node.filter(d => String(d.icon).includes('img/') || String(d.icon).includes('http://') || String(d.icon).includes('https://'))
    .append('image')
    .classed('node-icon', true)
    .attr('clip-path', d => `url(#clip-${d.id})`)
    .attr('xlink:href', d => d.icon)
    .attr('x', d => -d.radius * 0.7)
    .attr('y', d => -d.radius * 0.7)
    .attr('height', d => d.radius * 2 * 0.7)
    .attr('width', d => d.radius * 2 * 0.7)

  let legendOrdinal = legendColor()
    .scale(scaleColor)
    .shape('circle')

  svg.append('g')
    .classed('legend-color', true)
    .attr('text-anchor', 'start')
    .attr('transform', 'translate(20,30)')
    .style('font-size', '12px')
    .call(legendOrdinal)

  function ticked () {
    node
      .attr('transform', d => `translate(${d.x},${d.y})`)
      .select('circle')
      .attr('r', d => d.r)
  }
}

const Map = () => (
  <svg id="svg" width="100%" height="700" fontFamily="sans-serif" fontSize="12" textAnchor="middle"></svg>
)

export {
  Map,
  renderSvg,
}