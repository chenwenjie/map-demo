## Introduction
This is a web application built with `D3` & `React` to provide an interactive map of startup companies in Singapore.
- Map shows basic information of the companies i.e. Company Name, Tech Domain (Category) and Logo.
- Companies are grouped by tech domains and represented in different colors.
- Legend on the left connects tech domains to colors.<br>
- Data can be loaded from external sources or added manually.
- As a demonstration, two external data sources (Default & Cranchbase) are included.
New data sources can be easily imported with custom adapter.

## Table of Contents

- [Get Started](#get-started)
- [Load Data From Source](#load-data-from-source)
- [Add Company](#add-company)
- [Design Decisions](#design-decisions)

## Get Started

Make sure you have `npm` installed.

To start, run
```
npm start
```
Open web browser and go to
```
http://localhost:3000/
```
Enjoy!

## Load Data From Source
Two data sources are provided as examples to demonstrate how to load new data source into the map.
* `Default.json` and `Crunchbase.json` are the raw data files in different JSON format.
* `DefaultData.js` and `CrunchbaseData.json` are two adapters to transform raw data to the data format that this application accepts, which is
```javascript
[
  {
    'name': 'XXX',
    'cat': 'XXX',
    'icon': 'img/1.png',
    'value': 20,
  },
  ...
]
```

## Add Company
You can add a company manually. The following fields are provided:
1. Company Name
  * Displayed in the center of the circle when logo icon is not available
  * If too long, please add `;` in the middle to separate to multiple lines
2. Category
  * Used to group companies with different colors
  * Displayed on the legend
3. Display Size
  * Default value: 20
  * Increase or decrease if you want companies to show in different sizes
4. Icon Path (Optional)
  * Local path: please put the image file under `public/img/` and use `img/{IMAGE_FILE_NAME}`, i.e. `img/1.png`
  * Remote path: support any URL starts with `http://` or `https://`

## Design Decisions
The main purpose behind such design is to allow the tool to serve more diverse data, be portable, extensible and reusable. `D3` and `React` are chosen to achieve the goal.
* `D3` is the most power data visualisation tool that enables developers to build really complex and interactive charts. Using `D3` makes many future enhancements possible.
* `React` is a popular and powerful view library. With `react` component based architecture, it's easy to integrate this tool into the other `React` applications. Future changes can also be made easily.